import PostSchema from "../models/Post.js";

export const getLastTags = async (req, res) => {
    try {
        const posts = await PostSchema.find().limit(5).exec()

        const tags = posts
            .map((obj) => (obj.tags))
            .flat()
            .filter((value, index, self) => self.indexOf(value) === index)
            .slice(0, 5)

        res.json(tags)
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: 'Не удалось получить тэги',
        })
    }
}

export const getAll = async (req, res) => {
    try {
        const posts = await PostSchema.find().populate('user').exec()

        res.json(posts)
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: 'Не удалось получить статьи',
        })
    }
}

export const getOne = async (req, res) => {
    try {
        const postId = req.params.id;

        try {
            const doc = await PostSchema.findOneAndUpdate(
                { _id: postId },
                { $inc: { viewsCount: 1 } },
                { returnDocument: 'after' }
            );

            if (!doc) {
                return res.status(404).json({ message: 'Статья не найдена' });
            }

            return res.status(200).json(doc);
        } catch (err) {
            console.log(err);
            res.status(500).json({ message: 'Не удалось вернуть статью' });
        }

        res.json(posts)
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: 'Не удалось получить статьи',
        })
    }
}

export const remove = async (req, res) => {
    try {
        const postId = req.params.id;

        const doc = await PostSchema.findByIdAndDelete({ _id: postId })

        if (!doc) {
            return res.status(404).json({ message: 'Статья не найдена' });
        }

        res.json({
            success: true,
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: 'Не удалось удалить статью',
        })
    }
}

export const create = async (req, res) => {
    try {
        const doc = new PostSchema({
            title: req.body.title,
            text: req.body.text,
            tags: req.body.tags,
            imageUrl: req.body.imageUrl,
            user: req.userId,
        })

        const post = await doc.save();

        res.json(post)
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: 'Не удалось создать статью',
        })
    }
}

export const update = async (req, res) => {
    try {
        const postId = req.params.id

        await PostSchema.updateOne({ _id: postId }, {
            title: req.body.title,
            text: req.body.text,
            tags: req.body.tags,
            imageUrl: req.body.imageUrl,
            user: req.userId,
        })

        res.json({ success: true })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: 'Не удалось обновить статью'
        })
    }
}