import express from "express";
import mongoose from "mongoose";
import multer from "multer";
import cors from 'cors'

import { registerValidation, loginValidation, postCreateValidation } from './validation.js'
import { handleValidationErrors, checkAuth } from "./utils/index_utils.js"
import { UserController, PostController } from './controllers/index_Controller.js'

mongoose
    .connect('mongodb://127.0.0.1:27017/MERN_DB')
    .then(() => console.log("DB ok"))
    .catch(err => console.log("DB error", err))

const app = express()

app.get("/", (req, res) => {
    res.send("<h1>Hello!</h1>")
})

const storage = multer.diskStorage({
    destination: (_, __, cb) => { cb(null, 'uploads') },
    filename: (_, file, cb) => { cb(null, file.originalname) },
})

const upload = multer({ storage })

app.use(express.json())
app.use(cors())
app.use('/uploads/', express.static('uploads'))
app.post('/uploads/', checkAuth, upload.single('image'), (req, res) => {
    res.json({ url: `/uploads/${req.file.originalname}` })
})

app.post('/auth/login', loginValidation, handleValidationErrors, UserController.login)
app.post('/auth/register', registerValidation, handleValidationErrors, UserController.register)
app.get('/auth/me', checkAuth, UserController.getMe)

app.get('/tags', PostController.getLastTags)

app.get('/posts', PostController.getAll)
// app.get('/posts/tags', PostController.getLastTags)
app.get('/posts/:id', PostController.getOne)
app.post('/posts', checkAuth, postCreateValidation, handleValidationErrors, PostController.create)
app.patch('/posts/:id', checkAuth, postCreateValidation, handleValidationErrors, PostController.update)
app.delete('/posts/:id', checkAuth, PostController.remove)


app.listen(4444, (err) => {
    if (err) { return console.log(err) }
    console.log('Server OK')
})